<?php


namespace NoTee;


use NoTee\Nodes\TagNode;
use PHPUnit\Framework\TestCase;

class SubscriberDoingNothing implements SubscriberInterface
{
    public function notify(CreateTagEvent $event): TagNode
    {
        return $event->getNode();
    }
}

class PerformanceTest extends TestCase
{
    public function test()
    {
        $nf = new NodeFactory(new EscapingStrategy('utf-8'), new UriValidator(), new BlockManager());
        $nf->subscribe(new SubscriberDoingNothing());
        $start = microtime(true);
        $items = [];
        for($x = 0; $x < 1000; $x++) {
            $items[] = $nf->div(['class' => 'hello-world']);
        }
        $node = $nf->div($items);
        (string)$node;
        $end = microtime(true);
        $this->assertLessThan(0.030, $start - $end);
    }
}