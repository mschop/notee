<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class EscapingStrategyTest extends TestCase
{
    public function test_invalidEncoding_throwsException()
    {
        $this->expectException(InvalidArgumentException::class);
        new EscapingStrategy('utf--8');
    }
}