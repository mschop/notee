<?php

declare(strict_types=1);

namespace NoTee;


use NoTee\Nodes\TagNode;
use PHPUnit\Framework\TestCase;

class EventTestFormTagSubscriber implements SubscriberInterface
{
    public function notify(CreateTagEvent $event): TagNode
    {
        $node = $event->getNode();
        if ($node->getTagName() !== 'form') return $node;
        return $node->appendChild(
            $event->getNodeFactory()->input([
                'type' => 'hidden',
                'value' => '12345',
                'name' => 'xsrf_token',
            ]),
        );
    }
}

class EventTest extends TestCase
{
    public function test()
    {
        $nf = NoTee::create()->getNodeFactory();

        $expected = $nf->form(
            ['class' => 'someclass', 'id' => 'someid'],
            $nf->input(['type' => 'hidden', 'value' => '12345', 'name' => 'xsrf_token']),
        );

        $nf->subscribe(new EventTestFormTagSubscriber());

        $node = $nf->form(
            ['class' => 'someclass', 'id' => 'someid']
        );

        $this->assertEquals((string)$expected, (string)$node);

    }
}
