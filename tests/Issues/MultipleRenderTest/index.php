<?php

assert($noTee instanceof \NoTee\NoTee);
$blockManager = $noTee->getBlockManager();
$nodeFactory = $noTee->getNodeFactory();

$blockManager->extend('body', fn(callable $parent) => $nodeFactory->wrapper(
    $parent(),
    $nodeFactory->tag('div', ['Hello World']),
));

return $noTee->include('base.php', []);