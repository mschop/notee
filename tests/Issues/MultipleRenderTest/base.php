<?php

assert($noTee instanceof \NoTee\NoTee);
$nf = $noTee->getNodeFactory();

return $nf->div(
    $nf->block('body', fn() => $nf->wrapper()),
);
