<?php

namespace NoTee;

use PHPUnit\Framework\TestCase;


class MultipleRenderTest extends TestCase
{
    public function test()
    {
        $noTee = NoTee::create(templateDirs: [
            __DIR__ . '/MultipleRenderTest',
        ]);

        $result1 = $noTee->render('index.php');
        $result2 = $noTee->render('index.php');

        $expected = '<div><div>Hello World</div></div>';

        $this->assertEquals($expected, $result1, 'First call to render method returns correct markup');
        $this->assertEquals($expected, $result2, 'Second call to render method returns correct markup');
    }
}