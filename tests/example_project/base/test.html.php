<?php

/** @var string $defaultTitle */
/** @var string $titleExtension */

_extend('body', fn($parent) => _wrapper(
    $parent(),
    _text(' World'),
));

_extend('title', fn() => _text($defaultTitle . ' ' . $titleExtension));

return _include('base.html.php');