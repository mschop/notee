<?php

use NoTee\NoTeeInterface;

_include('defaults.php');

/** @var $noTee NoTeeInterface */

return _document(
    _html(
        _head(
            _block(
                'head',
                fn() => _wrapper(
                    _title(
                        $noTee->getNodeFactory()->block('title', fn() => _text('Some Title'))
                    )
                )
            ),
            _style(
                _block('style', fn() => _wrapper())
            )
        ),
        _body(
            _block('body', fn() => _text('Hello World'))
        )
    )
);