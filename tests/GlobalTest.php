<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;
use function _a;

class GlobalTest extends TestCase
{
    public function test()
    {
        $noTee = NoTee::create('utf-8', [], [], true);
        $noTee->enableGlobal();
        $a = _a(); $line = __LINE__;
        $this->assertEquals(__FILE__ . ':' . $line, $a->getAttributes()['data-source']);
        NoTee::$global = null;
    }
}