<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;

class ExtensibilityTest extends TestCase
{
    public function test()
    {
        $templateDirs = [
            __DIR__ . '/example_project/base',
            __DIR__ . '/example_project/first_child',
            __DIR__ . '/example_project/last_child',
        ];
        $noTee = NoTee::create('utf-8', $templateDirs, ['defaultTitle' => 'NoTee']);
        $noTee->enableGlobal();
        $node = $noTee->render('test.html.php', ['titleExtension' => 'is cool']);
        $this->assertEquals(
            (string)_document(
                _html(
                    _head(
                        _title('NoTee is cool'),
                        _style(_raw('/* Some styles */'))
                    ),
                    _body(
                        'Hello Hello World World World'
                    ),
                )
            ),
            (string)$node
        );
        NoTee::$global = null;
    }

    public function testContextConflictException()
    {
        $defaultContext = [
            'var1' => 1,
            'var2' => 2,
            'var3' => 3,
        ];
        $noTee = NoTee::create('utf-8', [], $defaultContext);
        $this->expectException(ContextConflictException::class);
        $noTee->render('irrelevant.php', [
            'var2' => 1,
        ]);
    }

    function testReservedVariableConflictException()
    {
        $this->expectException(ContextConflictException::class);
        NoTee::create('utf-8', [], ['noTee' => 'test']);
    }
}