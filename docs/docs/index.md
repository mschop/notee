# Setup

The only officially supported installation method is through Composer:

    composer require mschop/notee
    
If you don't have Composer available, follow the Composer installation instructions under:
[Composer Installation](https://getcomposer.org/doc/00-intro.md)

Next you need to include the file `vendor/autoload.php` provided by Composer.

    <?php
    require_once '--PATH_TO_ROOT--/vendor/autoload.php';
    
Now you can create an instance of the class `NoTee\NoTee`:

```php
    <?php
    $charset = 'utf-8';
    $templateDirs = []; // optional array of directories, used for the optional template functionality
    $defaultContext = []; // optional array of default context variables, used for all files in template functionality
    $debug = false;
    $noTee = \NoTee\NoTee::create($charset, $templateDirs, $defaultContext, $debug);
```

(optional) NoTee provides global functions, which make the template code much easier to read. Please note, that only one instance of the
NoTee class can be registered globally. Whether you use this functionality is a matter of taste and application architecture.

```php
    $noTee->enableGlobal();
```

# Usage

This guide assumes, that you called `$noTee->enableGlobal()`. In this case you can use the global functions. The coding style is slightly
different:

Without global functions:

```php
    $nf = $noTee->getNodeFactory();
    echo $nf->div("Hello World");
```
    
With global functions (recommended):

```php
    $noTee->enableGlobal();
    echo _div('Hello World');
```
    
Simple example:

```php
    echo _document(
        _html(
            _head(
                ...
            ),
            _body(
                ...
            ),
        )
    );
```
    
Example using the template system:

```php
    echo $noTee->getTemplate()->render('index.html.php');
```
