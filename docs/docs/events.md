You can register an event subscriber to NoTee. This subscriber can be a filter. With an event subscriber you can react
to node creation. The mechanism is very flexible. The following example demonstrates the functionality with a listener,
that adds in hidden csrf token to every insecure form.

```php
<?php

$noTee = NoTee\NoTee::create();
$noTee->getNodeFactory()->subscribe(new class() implements SubscriberInterface {
    public function notify(CreateTagEvent $event): DefaultNode;
    {
        $node = $event->getNode();
        if ($node->getTagName() === 'form') {
            $csrfHiddenInput = $event->getNodeFactory()->input([
                'type' => 'hidden',
                'name' => 'csrf_token',
                'value' => 'secure_token_1234566',
            ]);
            $node = $node->appendChild($csrfHiddenInput);
        }
        return $node;
    }
});
```
