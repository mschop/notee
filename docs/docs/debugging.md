NoTee has an awesome debug feature. If enabled, an additional attribute is added to all generated html nodes. This
attribute contains the source file and line, the html node is coming from:

```html
`<a href="" data-source="/var/www/template/index.html.php:99">Link</a>`
```

To enable debug mode, you need to pass the `$debug` parameter to NoTee instance creation with value true.

```php
    $noTee = NoTee\NoTee::create('utf-8', [], [], true);
```