<?php

declare(strict_types=1);

namespace NoTee;


use NoTee\Nodes\BlockNode;
use NoTee\Nodes\TagNode;
use NoTee\Nodes\DocumentNode;
use NoTee\Nodes\RawNode;
use NoTee\Nodes\TextNode;
use NoTee\Nodes\WrapperNode;

interface NodeFactoryInterface
{
    function subscribe(SubscriberInterface $subscriber): void;
    function tag(string $name, array $arguments): TagNode;
    function text(string $text): TextNode;
    function raw(string $raw): RawNode;
    function document(NodeInterface $root): DocumentNode;
    function block(string $name, callable $implementation): BlockNode;
    function wrapper(...$args): WrapperNode;
    function __call($name, $arguments): TagNode;
}