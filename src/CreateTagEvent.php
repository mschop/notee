<?php

declare(strict_types=1);

namespace NoTee;


use NoTee\Nodes\TagNode;

class CreateTagEvent
{
    public function __construct(
        protected NodeFactoryInterface $nodeFactory,
        protected TagNode $node,
    )
    {
    }

    public function getNodeFactory(): NodeFactoryInterface
    {
        return $this->nodeFactory;
    }

    public function getNode(): TagNode
    {
        return $this->node;
    }
}
