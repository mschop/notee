<?php

declare(strict_types=1);

namespace NoTee;

use Stringable;

interface NodeInterface extends Stringable
{
}
