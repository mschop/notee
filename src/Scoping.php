<?php

declare(strict_types=1);

namespace NoTee;


class Scoping
{
    public static function requireScoped(string $file, array $context)
    {
        foreach ($context as $key => $value) {
            $$key = $value;
        }
        unset($context);
        return require($file);
    }
}