<?php

declare(strict_types=1);

namespace NoTee;


interface UriValidatorInterface
{
    public function isValid(string $uri): bool;
}