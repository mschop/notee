<?php


namespace NoTee\Nodes;


use NoTee\EscapingStrategyInterface;
use NoTee\NodeInterface;

class TagNode implements NodeInterface
{
    // Void tags are html tags, that are not allowed to have child elements.
    public const VOID_TAGS = [
        'area',
        'base',
        'br',
        'col',
        'command',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'track',
        'wbr',
    ];

    /**
     * TagNode constructor.
     * @param string $tagName
     * @param EscapingStrategyInterface $escapingStrategy
     * @param array $attributes
     * @param NodeInterface[] $children
     */
    public function __construct(
        protected string $tagName,
        protected EscapingStrategyInterface $escapingStrategy,
        protected array $attributes = [],
        protected array $children = [],
    ) { }

    public function __toString(): string
    {
        $attributeString = !empty($this->attributes) ? ' ' . $this->getAttributeString() : '';

        if (in_array($this->tagName, static::VOID_TAGS)) {
            if (!empty($this->children)){
                throw new \Exception('Void tags cannot have children');
            }
            return "<{$this->tagName}{$attributeString}/>";
        }

        $children = implode('', $this->children);
        return "<{$this->tagName}$attributeString>$children</{$this->tagName}>";
    }

    public function getAttributeString(): string
    {
        $attributeString = '';
        $first = true;
        foreach ($this->attributes as $name => $value) {
            $escapedAttribute = $this->escapeAttribute($value);
            $attributeString .= ($first ? '' : ' ') . $name . '="' . $escapedAttribute . '"';
            $first = false;
        }
        return $attributeString;
    }

    /**
     * @return string
     */
    public function getTagName(): string
    {
        return $this->tagName;
    }

    /**
     * @return EscapingStrategyInterface
     */
    public function getEscapingStrategy(): EscapingStrategyInterface
    {
        return $this->escapingStrategy;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @return NodeInterface[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    protected function escapeAttribute(string $value): string
    {
        return $this->escapingStrategy->escapeAttribute($value);
    }

    // MANIPULATION FUNCTIONS

    /**
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function setAttribute(string $key, string $value): static
    {
        $attributes = $this->attributes;
        $attributes[$key] = $value;
        return new static($this->tagName, $this->escapingStrategy, $attributes, $this->children);
    }

    public function appendChild(NodeInterface $node): static
    {
        $children = $this->children;
        $children[] = $node;
        return new static($this->tagName, $this->escapingStrategy, $this->attributes, $children);
    }
}
