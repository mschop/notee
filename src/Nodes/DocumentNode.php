<?php

declare(strict_types=1);

namespace NoTee\Nodes;


use NoTee\NodeInterface;

class DocumentNode implements NodeInterface
{
    public function __construct(
        protected NodeInterface $topNode,
    )
    {
    }

    public function __toString(): string
    {
        return "<!DOCTYPE html>{$this->topNode}";
    }
}
