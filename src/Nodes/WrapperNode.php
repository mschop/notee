<?php

namespace NoTee\Nodes;


use NoTee\EscapingStrategyInterface;
use NoTee\NodeInterface;

class WrapperNode implements NodeInterface
{
    /**
     * WrapperNode constructor.
     * @param NodeInterface[] $children
     */
    public function __construct(
        protected array $children
    )
    {
    }

    public function __toString() : string
    {
        return implode('', $this->children);
    }
}
