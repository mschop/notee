<?php

declare(strict_types=1);

namespace NoTee\Nodes;

use NoTee\EscapingStrategyInterface;
use NoTee\NodeInterface;

class TextNode implements NodeInterface
{
    public function __construct(
        protected string $text,
        protected EscapingStrategyInterface $escapingStrategy,
    )
    {
    }

    public function __toString(): string
    {
        return $this->escapingStrategy->escapeHtml($this->text);
    }
}
