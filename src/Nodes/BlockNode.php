<?php


namespace NoTee\Nodes;


use NoTee\BlockManagerInterface;
use NoTee\NodeInterface;

class BlockNode implements NodeInterface
{
    public function __construct(
        protected BlockManagerInterface $blockManager,
        protected string $name,
    )
    {
    }

    public function __toString(): string
    {
        return (string)$this->blockManager->compose($this->name);
    }
}
