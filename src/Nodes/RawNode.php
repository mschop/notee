<?php

declare(strict_types=1);

namespace NoTee\Nodes;

use NoTee\NodeInterface;

class RawNode implements NodeInterface
{
    public function __construct(
        protected string $raw,
    ) { }

    public function __toString(): string
    {
        return $this->raw;
    }
}
