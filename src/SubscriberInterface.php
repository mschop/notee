<?php

declare(strict_types=1);

namespace NoTee;


use NoTee\Nodes\TagNode;

interface SubscriberInterface
{
    public function notify(CreateTagEvent $event): TagNode;
}