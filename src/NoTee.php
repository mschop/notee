<?php

declare(strict_types=1);

namespace NoTee;


use Exception;
use InvalidArgumentException;

class NoTee implements NoTeeInterface
{
    public static ?NoTeeInterface $global = null;

    /**
     * NoTee constructor.
     * @param NodeFactoryInterface $nodeFactory
     * @param BlockManagerInterface $blockManager
     * @param array $templateDirs
     * @param array $defaultContext
     * @throws ContextConflictException
     */
    public function __construct(
        protected NodeFactoryInterface $nodeFactory,
        protected BlockManagerInterface $blockManager,
        protected array $templateDirs = [],
        protected array $defaultContext =  []
    )
    {
        if (isset($defaultContext['noTee'])) {
            throw new ContextConflictException('The context variable "noTee" is reserved.');
        }
        $this->defaultContext['noTee'] = $this;
    }

    /**
     * @param string $charset
     * @param array $templateDirs
     * @param array $defaultContext
     * @param bool $debug
     * @return static
     * @throws ContextConflictException
     */
    static function create(string $charset = 'utf-8', array $templateDirs = [], array $defaultContext = [], bool $debug = false): self
    {
        $blockManager = new BlockManager();
        $nodeFactory = new NodeFactory(new EscapingStrategy($charset), new UriValidator(), $blockManager, $debug);
        return new static($nodeFactory, $blockManager, $templateDirs, $defaultContext);
    }

    function getNodeFactory(): NodeFactoryInterface
    {
        return $this->nodeFactory;
    }

    /**
     * @param string $relativeFilePath
     * @param array $context
     * @return NodeInterface
     * @throws ContextConflictException
     */
    function render(string $relativeFilePath, array $context = []): NodeInterface
    {
        $this->blockManager->reset();
        return $this->include($relativeFilePath, $context);
    }

    /**
     * @param string $relativeFilePath
     * @param array $context
     * @return NodeInterface
     * @throws ContextConflictException
     */
    public function include(string $relativeFilePath, array $context): NodeInterface
    {
        $contextIntersect = array_intersect(array_keys($this->defaultContext), array_keys($context));
        if (!empty($contextIntersect)) {
            $intersectString = implode(',', $contextIntersect);
            throw new ContextConflictException("Context variables are conflicting with global context variables: $intersectString");
        }
        $context = array_merge($this->defaultContext, $context);
        $relativeFilePath = trim($relativeFilePath, '/\\');
        $dirs = array_reverse($this->templateDirs);
        foreach ($dirs as $dir) {
            $path = $dir . '/' . $relativeFilePath;
            if (file_exists($path)) {
                $return = Scoping::requireScoped($path, $context);
                if ($return === null) {
                    return $this->nodeFactory->wrapper();
                } elseif ($return !== 1) {
                    return $return;
                }
            }
        }
        throw new InvalidArgumentException("Template File '$relativeFilePath' either does not exist or does not return an instance of " . NodeInterface::class);
    }

    function getBlockManager(): BlockManagerInterface
    {
        return $this->blockManager;
    }

    /**
     * @throws Exception
     */
    function enableGlobal(): void
    {
        if (static::$global !== null && static::$global !== $this) {
            $class = NoTee::class;
            $msg = "You cannot use this instance of $class as global instance, because another instance is already registered";
            throw new Exception($msg);
        }
        static::$global = $this;
        require_once __DIR__ . '/../global-functions.php';
    }
}