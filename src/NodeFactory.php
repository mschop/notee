<?php

namespace NoTee;

use InvalidArgumentException;
use NoTee\Nodes\BlockNode;
use NoTee\Nodes\TagNode;
use NoTee\Nodes\DocumentNode;
use NoTee\Nodes\RawNode;
use NoTee\Nodes\TextNode;
use NoTee\Nodes\WrapperNode;

class NodeFactory implements NodeFactoryInterface
{
    protected const URI_ATTRIBUTES = [
        'action' => true,
        'archive' => true,
        'cite' => true,
        'classid' => true,
        'codebase' => true,
        'data' => true,
        'formaction' => true,
        'href' => true,
        'icon' => true,
        'longdesc' => true,
        'manifest' => true,
        'poster' => true,
        'src' => true,
        'usemap' => true,
    ];

    /** @var SubscriberInterface[] */
    protected array $subscriber = [];

    public function __construct(
        readonly protected EscapingStrategyInterface $escapingStrategy,
        readonly protected UriValidatorInterface $uriValidator,
        readonly protected BlockManagerInterface $blockManager,
        readonly protected bool $debug = false
    ) { }

    /**
     * @param string $name
     * @param array $arguments
     * @return TagNode
     * @throws InvalidArgumentException
     */
    public function tag(string $name, array $arguments): TagNode
    {
        $attributes = [];
        if ($this->debug) $attributes['data-source'] = $this->generateDebugSource();

        $firstArgument = reset($arguments);
        if (is_array($firstArgument)) {
            $firstAttribute = reset($firstArgument);
            if ($firstAttribute !== null && !$firstAttribute instanceof NodeInterface) {
                $attributes = array_merge($firstArgument, $attributes);
                $this->validateAttributes($attributes);
                // remove the first element (because it contains the attributes)
                array_shift($arguments);
            }
        }

        $children = static::flatten($arguments);
        $this->replaceStringLiteralsWithTextNodes($children);

        return $this->notify(
            new TagNode(
                $name,
                $this->escapingStrategy,
                $attributes,
                $children,
            )
        );
    }

    /**
     * Get information on where a node has been created
     * @return string
     */
    protected function generateDebugSource(): string
    {
        $srcDir = __DIR__;
        $globalFile = realpath(__DIR__ . '/../global-functions.php');
        foreach (debug_backtrace() as $callee) {
            $file = $callee['file'];
            if (strpos($file, $srcDir) === false && strpos($file, $globalFile) === false) {
                return $file . ':' . $callee['line'];
            }
        }
        return '';
    }

    /**
     * @param array $attributes
     * @throws InvalidArgumentException
     */
    protected function validateAttributes(array $attributes): void
    {
        foreach ($attributes as $key => $value) {
            if (!$this->isValidAttributeKey($key)) {
                throw new \InvalidArgumentException('invalid attribute name ' . $key);
            }
            if (!$this->isValidAttributeValue($key, $value)) {
                throw new \InvalidArgumentException('invalid attribute value for ' . $key);
            }
        }
    }

    protected function isValidAttributeKey(string $key): bool
    {
        if (!preg_match('/^[0-9a-z-_]*$/i', $key)) {
            return false;
        }
        return true;
    }

    protected function isValidAttributeValue(string $key, string $value): bool
    {
        if (array_key_exists($key, static::URI_ATTRIBUTES)) {
            return $this->uriValidator->isValid($value);
        }
        return true;
    }

    /**
     * An api consumer can pass arrays coming from function calls as children to the method "create". Elements in this
     * array are direct children of the node created with the method "create". Those must therefore be flattened.
     * @param array $arguments
     * @return array
     */
    protected static function flatten(array $arguments): array
    {
        $result = [];
        foreach ($arguments as $argument) {
            if (is_array($argument)) {
                $result = array_merge($result, $argument);
            } elseif ($argument !== null) {
                $result[] = $argument;
            }
        }
        return $result;
    }

    /**
     * Notify all subscriber
     *
     * @param TagNode $node
     * @return TagNode
     */
    protected function notify(TagNode $node): TagNode
    {
        foreach ($this->subscriber as $subscriber) {
            $node = $subscriber->notify(new CreateTagEvent($this, $node));
        }

        return $node;
    }

    /**
     * Add an event listener. The event listener is called when a new DefaultNode is created.
     *
     * @param SubscriberInterface $subscriber
     */
    public function subscribe(SubscriberInterface $subscriber): void
    {
        $this->subscriber[] = $subscriber;
    }

    /**
     * Output escaped text.
     *
     * @param string $text
     * @return TextNode
     */
    public function text(string $text): TextNode
    {
        return new TextNode($text, $this->escapingStrategy);
    }

    /**
     * This method creates a RawNode instance. RawNode is used to output unescaped html content.
     *
     * @param string $raw
     * @return RawNode
     */
    public function raw(string $raw): RawNode
    {
        return new RawNode($raw);
    }

    /**
     * The Document-Node is used for creating the DOCTYPE. Even if the html representation of the doctype does not
     * contain any nodes, NodeFactory expects the Document-Node to contain all other nodes.
     *
     * @param TagNode $root
     * @return DocumentNode
     */
    public function document(NodeInterface $root): DocumentNode
    {
        return new DocumentNode($root);
    }

    public function block(string $name, callable $implementation): BlockNode
    {
        $this->blockManager->define($name, $implementation);
        return new BlockNode($this->blockManager, $name);
    }

    /**
     * Creates a wrapper, that does not produce any HTML. This method is useful for cases, where you need to output
     * multiple nodes.
     *
     * Example:
     *
     * $values = [1, 2, 3];
     * $nodeFactory->ul(
     *   $nodeFactory->wrapper(array_map(function ($value) { return $nodeFactory->li($value); })
     * )
     * @var array $args
     * @return WrapperNode
     */
    public function wrapper(...$args): WrapperNode
    {
        $args = static::flatten($args);
        $this->replaceStringLiteralsWithTextNodes($args);
        return new WrapperNode($args);
    }

    /**
     * It is allowed to put string literals into the array of child nodes. Those should be converted to TextNode instances.
     *
     * @param array &$arguments
     * @return void
     */
    protected function replaceStringLiteralsWithTextNodes(array &$arguments): void
    {
        foreach($arguments as &$argument) {
            if (!is_object($argument) && $argument !== null) {
                $argument = new TextNode($argument, $this->escapingStrategy);
            }
        }
    }

    /**
     * This method enables to use custom tag names, which are not defined below.
     *
     * @param $name
     * @param $arguments
     * @return TagNode
     */
    public function __call($name, $arguments): TagNode
    {
        return $this->tag($name, $arguments);
    }

    public function a(...$args) : TagNode { return $this->tag('a', $args); }
    public function abbr(...$args) : TagNode { return $this->tag('abbr', $args); }
    public function acronym(...$args) : TagNode { return $this->tag('acronym', $args); }
    public function address(...$args) : TagNode { return $this->tag('address', $args); }
    public function applet(...$args) : TagNode { return $this->tag('applet', $args); }
    public function area(...$args) : TagNode { return $this->tag('area', $args); }
    public function article(...$args) : TagNode { return $this->tag('article', $args); }
    public function aside(...$args) : TagNode { return $this->tag('aside', $args); }
    public function audio(...$args) : TagNode { return $this->tag('audio', $args); }
    public function base(...$args) : TagNode { return $this->tag('base', $args); }
    public function basefont(...$args) : TagNode { return $this->tag('basefont', $args); }
    public function b(...$args) : TagNode { return $this->tag('b', $args); }
    public function bdo(...$args) : TagNode { return $this->tag('bdo', $args); }
    public function bgsound(...$args) : TagNode { return $this->tag('bgsound', $args); }
    public function big(...$args) : TagNode { return $this->tag('big', $args); }
    public function blink(...$args) : TagNode { return $this->tag('blink', $args); }
    public function blockquote(...$args) : TagNode { return $this->tag('blockquote', $args); }
    public function body(...$args) : TagNode { return $this->tag('body', $args); }
    public function br(...$args) : TagNode { return $this->tag('br', $args); }
    public function button(...$args) : TagNode { return $this->tag('button', $args); }
    public function canvas(...$args) : TagNode { return $this->tag('canvas', $args); }
    public function caption(...$args) : TagNode { return $this->tag('caption', $args); }
    public function center(...$args) : TagNode { return $this->tag('center', $args); }
    public function cite(...$args) : TagNode { return $this->tag('cite', $args); }
    public function code(...$args) : TagNode { return $this->tag('code', $args); }
    public function col(...$args) : TagNode { return $this->tag('col', $args); }
    public function colgroup(...$args) : TagNode { return $this->tag('colgroup', $args); }
    public function command(...$args) : TagNode { return $this->tag('command', $args); }
    public function datalist(...$args) : TagNode { return $this->tag('datalist', $args); }
    public function dd(...$args) : TagNode { return $this->tag('dd', $args); }
    public function del(...$args) : TagNode { return $this->tag('del', $args); }
    public function details(...$args) : TagNode { return $this->tag('details', $args); }
    public function dfn(...$args) : TagNode { return $this->tag('dfn', $args); }
    public function div(...$args) : TagNode { return $this->tag('div', $args); }
    public function dl(...$args) : TagNode { return $this->tag('dl', $args); }
    public function dt(...$args) : TagNode { return $this->tag('dt', $args); }
    public function embed(...$args) : TagNode { return $this->tag('embed', $args); }
    public function em(...$args) : TagNode { return $this->tag('em', $args); }
    public function fieldset(...$args) : TagNode { return $this->tag('fieldset', $args); }
    public function figcaption(...$args) : TagNode { return $this->tag('figcaption', $args); }
    public function figure(...$args) : TagNode { return $this->tag('figure', $args); }
    public function font(...$args) : TagNode { return $this->tag('font', $args); }
    public function footer(...$args) : TagNode { return $this->tag('footer', $args); }
    public function form(...$args) : TagNode { return $this->tag('form', $args); }
    public function frame(...$args) : TagNode { return $this->tag('frame', $args); }
    public function frameset(...$args) : TagNode { return $this->tag('frameset', $args); }
    public function h1(...$args) : TagNode { return $this->tag('h1', $args); }
    public function h2(...$args) : TagNode { return $this->tag('h2', $args); }
    public function h3(...$args) : TagNode { return $this->tag('h3', $args); }
    public function h4(...$args) : TagNode { return $this->tag('h4', $args); }
    public function h5(...$args) : TagNode { return $this->tag('h5', $args); }
    public function h6(...$args) : TagNode { return $this->tag('h6', $args); }
    public function header(...$args) : TagNode { return $this->tag('header', $args); }
    public function head(...$args) : TagNode { return $this->tag('head', $args); }
    public function hgroup(...$args) : TagNode { return $this->tag('hgroup', $args); }
    public function hr(...$args) : TagNode { return $this->tag('hr', $args); }
    public function html(...$args) : TagNode { return $this->tag('html', $args); }
    public function iframe(...$args) : TagNode { return $this->tag('iframe', $args); }
    public function i(...$args) : TagNode { return $this->tag('i', $args); }
    public function img(...$args) : TagNode { return $this->tag('img', $args); }
    public function input(...$args) : TagNode { return $this->tag('input', $args); }
    public function ins(...$args) : TagNode { return $this->tag('ins', $args); }
    public function isindex(...$args) : TagNode { return $this->tag('isindex', $args); }
    public function kbd(...$args) : TagNode { return $this->tag('kbd', $args); }
    public function keygen(...$args) : TagNode { return $this->tag('keygen', $args); }
    public function label(...$args) : TagNode { return $this->tag('label', $args); }
    public function legend(...$args) : TagNode { return $this->tag('legend', $args); }
    public function li(...$args) : TagNode { return $this->tag('li', $args); }
    public function link(...$args) : TagNode { return $this->tag('link', $args); }
    public function listing(...$args) : TagNode { return $this->tag('listing', $args); }
    public function main(...$args) : TagNode { return $this->tag('main', $args); }
    public function map(...$args) : TagNode { return $this->tag('map', $args); }
    public function mark(...$args) : TagNode { return $this->tag('mark', $args); }
    public function marquee(...$args) : TagNode { return $this->tag('marquee', $args); }
    public function math(...$args) : TagNode { return $this->tag('math', $args); }
    public function menu(...$args) : TagNode { return $this->tag('menu', $args); }
    public function meta(...$args) : TagNode { return $this->tag('meta', $args); }
    public function meter(...$args) : TagNode { return $this->tag('meter', $args); }
    public function nav(...$args) : TagNode { return $this->tag('nav', $args); }
    public function nextid(...$args) : TagNode { return $this->tag('nextid', $args); }
    public function nobr(...$args) : TagNode { return $this->tag('nobr', $args); }
    public function noembed(...$args) : TagNode { return $this->tag('noembed', $args); }
    public function noframes(...$args) : TagNode { return $this->tag('noframes', $args); }
    public function noscript(...$args) : TagNode { return $this->tag('noscript', $args); }
    public function object(...$args) : TagNode { return $this->tag('object', $args); }
    public function ol(...$args) : TagNode { return $this->tag('uol', $args); }
    public function optgroup(...$args) : TagNode { return $this->tag('optgroup', $args); }
    public function option(...$args) : TagNode { return $this->tag('option', $args); }
    public function output(...$args) : TagNode { return $this->tag('output', $args); }
    public function param(...$args) : TagNode { return $this->tag('param', $args); }
    public function plaintext(...$args) : TagNode { return $this->tag('plaintext', $args); }
    public function p(...$args) : TagNode { return $this->tag('p', $args); }
    public function pre(...$args) : TagNode { return $this->tag('pre', $args); }
    public function progress(...$args) : TagNode { return $this->tag('progress', $args); }
    public function q(...$args) : TagNode { return $this->tag('q', $args); }
    public function rp(...$args) : TagNode { return $this->tag('rp', $args); }
    public function rt(...$args) : TagNode { return $this->tag('rt', $args); }
    public function ruby(...$args) : TagNode { return $this->tag('ruby', $args); }
    public function samp(...$args) : TagNode { return $this->tag('samp', $args); }
    public function script(...$args) : TagNode { return $this->tag('script', $args); }
    public function section(...$args) : TagNode { return $this->tag('section', $args); }
    public function select(...$args) : TagNode { return $this->tag('select', $args); }
    public function small(...$args) : TagNode { return $this->tag('small', $args); }
    public function source(...$args) : TagNode { return $this->tag('source', $args); }
    public function spacer(...$args) : TagNode { return $this->tag('spacer', $args); }
    public function span(...$args) : TagNode { return $this->tag('span', $args); }
    public function s(...$args) : TagNode { return $this->tag('s', $args); }
    public function strike(...$args) : TagNode { return $this->tag('strike', $args); }
    public function strong(...$args) : TagNode { return $this->tag('strong', $args); }
    public function style(...$args) : TagNode { return $this->tag('style', $args); }
    public function sub(...$args) : TagNode { return $this->tag('sub', $args); }
    public function sup(...$args) : TagNode { return $this->tag('sup', $args); }
    public function summary(...$args) : TagNode { return $this->tag('summary', $args); }
    public function svg(...$args) : TagNode { return $this->tag('svg', $args); }
    public function table(...$args) : TagNode { return $this->tag('table', $args); }
    public function tbody(...$args) : TagNode { return $this->tag('tbody', $args); }
    public function td(...$args) : TagNode { return $this->tag('td', $args); }
    public function textarea(...$args) : TagNode { return $this->tag('textarea', $args); }
    public function tfoot(...$args) : TagNode { return $this->tag('tfoot', $args); }
    public function thead(...$args) : TagNode { return $this->tag('thead', $args); }
    public function th(...$args) : TagNode { return $this->tag('th', $args); }
    public function time(...$args) : TagNode { return $this->tag('time', $args); }
    public function title(...$args) : TagNode { return $this->tag('title', $args); }
    public function tr(...$args) : TagNode { return $this->tag('tr', $args); }
    public function tt(...$args) : TagNode { return $this->tag('tt', $args); }
    public function ul(...$args) : TagNode { return $this->tag('ul', $args); }
    public function u(...$args) : TagNode { return $this->tag('u', $args); }
    public function var(...$args) : TagNode { return $this->tag('var', $args); }
    public function video(...$args) : TagNode { return $this->tag('video', $args); }
    public function wbr(...$args) : TagNode { return $this->tag('wbr', $args); }
    public function xmp(...$args) : TagNode { return $this->tag('xmp', $args); }
}
