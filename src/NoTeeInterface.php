<?php

declare(strict_types=1);

namespace NoTee;


interface NoTeeInterface
{
    public function getNodeFactory(): NodeFactoryInterface;
    public function render(string $relativeFilePath, array $context = []): NodeInterface;
    public function include(string $relativeFilePath, array $context): NodeInterface;
    public function enableGlobal(): void;
}