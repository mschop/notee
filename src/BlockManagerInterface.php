<?php

declare(strict_types=1);

namespace NoTee;


interface BlockManagerInterface
{
    /**
     * This method should be used, to define a new block.
     *
     * @param string $name
     * @param callable $callable
     */
    public function define(string $name, callable $callable): void;

    /**
     * This method should be used, to extend an existing block.
     *
     * @param string $name
     * @param callable $callable
     */
    public function extend(string $name, callable $callable): void;

    /**
     * This method composes a specific block. It returns the resulting NodeInterface-Tree.
     *
     * @param string $name
     * @return NodeInterface
     */
    public function compose(string $name): NodeInterface;

    /**
     * Resets all blocks and extends. This is required e.g. for the render process, because
     * otherwise an exception is thrown when multiple render calls are executed.
     *
     * @return void
     */
    public function reset(): void;
}