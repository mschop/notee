#!/usr/bin/env bash

set -e

docker login registry.gitlab.com
docker build --build-arg PHP_VERSION=8.2 -t registry.gitlab.com/mschop/notee .
docker push registry.gitlab.com/mschop/notee