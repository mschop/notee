#!/usr/bin/env bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker=`command -v docker`

if [ -z "$docker" ]; then
    printf "\nDocker is missing from your installation.\n"
    exit 1
fi

mutations=NO

for option in "$@"; do
  case $option in
    --mutations)
      mutations=YES
      ;;
  esac
done

versions=( "8.1" "8.2" "8.3" "8.4" )

for version in "${versions[@]}" # Later add further versions here
do
    versionLower=$(echo $version | tr '[:upper:]' '[:lower:]')
    dockerTag="notee-php-$versionLower"
    docker build "$DIR" -t "$dockerTag" --build-arg "PHP_VERSION=$version"
    docker run -v "$DIR:/code" -w "/code" "$dockerTag" composer update --no-interaction --no-progress

    if [[ $mutations == "YES" ]]; then
        docker run -v "$DIR:/code" -w "/code" "$dockerTag" vendor/bin/infection
    else
        docker run -v "$DIR:/code" -w "/code" "$dockerTag" vendor/bin/phpunit
    fi
done