<?php

use NoTee\NoTee;
use NoTee\NodeInterface;
use NoTee\Nodes\BlockNode;
use NoTee\Nodes\TagNode;
use NoTee\Nodes\DocumentNode;
use NoTee\Nodes\RawNode;
use NoTee\Nodes\TextNode;
use NoTee\Nodes\WrapperNode;


function _node(string $name, ...$args): TagNode
{
    return call_user_func_array(
        [NoTee::$global->getNodeFactory(), 'tag'],
        array_merge([$name], $args),
    );
}

function _raw(string $text): RawNode
{
    return NoTee::$global->getNodeFactory()->raw($text);
}

function _text(string $text): TextNode
{
    return NoTee::$global->getNodeFactory()->text($text);
}

function _wrapper(...$args): WrapperNode
{
    return call_user_func_array([NoTee::$global->getNodeFactory(), 'wrapper'], $args);
}

function _document(NodeInterface $topNode): DocumentNode
{
    return NoTee::$global->getNodeFactory()->document($topNode);
}

function _block(string $name, callable $callable): BlockNode
{
    return NoTee::$global->getNodeFactory()->block($name, $callable);
}

function _extend(string $name, callable $callable): void
{
    NoTee::$global->getBlockManager()->extend($name, $callable);
}

function _include(string $file, array $context = []): NodeInterface
{
    return NoTee::$global->include($file, $context);
}

function _a(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('a', $args);
}

function _abbr(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('abbr', $args);
}

function _acronym(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('acronym', $args);
}

function _address(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('address', $args);
}

function _applet(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('applet', $args);
}

function _area(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('area', $args);
}

function _article(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('article', $args);
}

function _aside(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('aside', $args);
}

function _audio(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('audio', $args);
}

function _base(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('base', $args);
}

function _basefont(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('basefont', $args);
}

function _b(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('b', $args);
}

function _bdo(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('bdo', $args);
}

function _bgsound(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('bgsound', $args);
}

function _big(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('big', $args);
}

function _blink(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('blink', $args);
}

function _blockquote(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('blockquote', $args);
}

function _body(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('body', $args);
}

function _br(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('br', $args);
}

function _button(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('button', $args);
}

function _canvas(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('canvas', $args);
}

function _caption(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('caption', $args);
}

function _center(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('center', $args);
}

function _cite(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('cite', $args);
}

function _code(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('code', $args);
}

function _col(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('col', $args);
}

function _colgroup(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('colgroup', $args);
}

function _command(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('command', $args);
}

function _datalist(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('datalist', $args);
}

function _dd(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('dd', $args);
}

function _del(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('del', $args);
}

function _details(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('details', $args);
}

function _dfn(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('dfn', $args);
}

function _div(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('div', $args);
}

function _dl(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('dl', $args);
}

function _dt(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('dt', $args);
}

function _embed(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('embed', $args);
}

function _em(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('em', $args);
}

function _fieldset(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('fieldset', $args);
}

function _figcaption(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('figcaption', $args);
}

function _figure(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('figure', $args);
}

function _font(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('font', $args);
}

function _footer(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('footer', $args);
}

function _form(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('form', $args);
}

function _frame(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('frame', $args);
}

function _frameset(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('frameset', $args);
}

function _h1(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h1', $args);
}

function _h2(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h2', $args);
}

function _h3(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h3', $args);
}

function _h4(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h4', $args);
}

function _h5(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h5', $args);
}

function _h6(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('h6', $args);
}

function _header(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('header', $args);
}

function _head(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('head', $args);
}

function _hgroup(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('hgroup', $args);
}

function _hr(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('hr', $args);
}

function _html(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('html', $args);
}

function _iframe(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('iframe', $args);
}

function _i(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('i', $args);
}

function _img(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('img', $args);
}

function _input(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('input', $args);
}

function _ins(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('ins', $args);
}

function _isindex(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('isindex', $args);
}

function _kbd(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('kbd', $args);
}

function _keygen(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('keygen', $args);
}

function _label(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('label', $args);
}

function _legend(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('legend', $args);
}

function _li(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('li', $args);
}

function _link(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('link', $args);
}

function _listing(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('listing', $args);
}

function _main(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('main', $args);
}

function _map(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('map', $args);
}

function _mark(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('mark', $args);
}

function _marquee(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('marquee', $args);
}

function _math(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('math', $args);
}

function _menu(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('menu', $args);
}

function _meta(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('meta', $args);
}

function _meter(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('meter', $args);
}

function _nav(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('nav', $args);
}

function _nextid(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('nextid', $args);
}

function _nobr(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('nobr', $args);
}

function _noembed(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('noembed', $args);
}

function _noframes(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('noframes', $args);
}

function _noscript(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('noscript', $args);
}

function _object(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('object', $args);
}

function _ol(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('ol', $args);
}

function _optgroup(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('optgroup', $args);
}

function _option(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('option', $args);
}

function _output(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('output', $args);
}

function _param(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('param', $args);
}

function _plaintext(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('plaintext', $args);
}

function _p(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('p', $args);
}

function _pre(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('pre', $args);
}

function _progress(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('progress', $args);
}

function _q(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('q', $args);
}

function _rp(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('rp', $args);
}

function _rt(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('rt', $args);
}

function _ruby(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('ruby', $args);
}

function _samp(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('samp', $args);
}

function _script(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('script', $args);
}

function _section(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('section', $args);
}

function _select(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('select', $args);
}

function _small(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('small', $args);
}

function _source(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('source', $args);
}

function _spacer(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('spacer', $args);
}

function _span(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('span', $args);
}

function _s(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('s', $args);
}

function _strike(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('strike', $args);
}

function _strong(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('strong', $args);
}

function _style(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('style', $args);
}

function _sub(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('sub', $args);
}

function _sup(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('sup', $args);
}

function _summary(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('summary', $args);
}

function _svg(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('svg', $args);
}

function _table(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('table', $args);
}

function _tbody(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('tbody', $args);
}

function _td(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('td', $args);
}

function _textarea(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('textarea', $args);
}

function _tfoot(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('tfoot', $args);
}

function _thead(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('thead', $args);
}

function _th(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('th', $args);
}

function _time(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('time', $args);
}

function _title(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('title', $args);
}

function _tr(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('tr', $args);
}

function _tt(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('tt', $args);
}

function _ul(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('ul', $args);
}

function _u(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('u', $args);
}

function _var(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('var', $args);
}

function _video(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('video', $args);
}

function _wbr(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('wbr', $args);
}

function _xmp(...$args): TagNode
{
    return NoTee::$global->getNodeFactory()->tag('xmp', $args);
}
