#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
PHP_VERSION="8.3.0"

docker build -t notee --build-arg PHP_VERSION=$PHP_VERSION .
docker run -it -v "$DIR:/code" -w "/code" notee bash
